<?php

namespace BinaryStudioAcademy;


use BinaryStudioAcademy\Commands\GalaxyCommand;

class GalaxyInfo
{
    private $currentGalaxy;

    public function setGalaxy($gameCommand)
    {
       if ($gameCommand instanceof GalaxyCommand)
       {
           $this->currentGalaxy = $gameCommand;
       }

    }

    public function getGalaxy()
    {
        return $this->currentGalaxy;
    }
}