<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Factories\CommandFactory;
use BinaryStudioAcademy\Factories\GalaxyFactory;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Invoker;

class Game
{
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln("Hello! Let`s start! Type 'help' to see all commands");
        $input = trim($reader->read());

        $commandFactory = new CommandFactory(new GalaxyFactory($this->getRandom()));
        $gameCommand = $commandFactory->createMethod($input);

        $invoker = new Invoker();
        $invoker->setCommand($gameCommand);

        $writer->writeln($invoker->run());
    }

    public function getRandom()
    {
        return $this->random->get();
    }

    public function run(Reader $reader, Writer $writer)
    {

    }

}
