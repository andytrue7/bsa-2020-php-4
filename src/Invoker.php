<?php

namespace BinaryStudioAcademy;

use BinaryStudioAcademy\Commands\GalaxyCommand;
use BinaryStudioAcademy\Game\Game;

class Invoker
{
    private $command;

    public function setCommand($command)
    {
        $this->command = $command;
    }

    public function run()
    {
        return $this->command->execute();
    }
}