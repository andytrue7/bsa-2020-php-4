<?php

namespace BinaryStudioAcademy\Commands;

use BinaryStudioAcademy\Game\Game;
class GalaxyCommand implements Command
{
    private $galaxy;

    public function __construct($galaxy)
    {
        $this->galaxy = $galaxy;
    }

    public function execute()
    {
      return  $this->galaxy->render();
    }

}