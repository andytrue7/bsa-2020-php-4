<?php

namespace BinaryStudioAcademy\Commands;

interface Command
{
    public function execute();
}