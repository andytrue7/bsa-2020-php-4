<?php

namespace BinaryStudioAcademy\Commands;

class HelpCommand implements Command
{
    private $helpReceiver;

    public function __construct($helpReceiver)
    {
        $this->helpReceiver = $helpReceiver;
    }

    public function execute()
    {
        return $this->helpReceiver->renderCommands();
    }

}