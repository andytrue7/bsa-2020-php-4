<?php

namespace BinaryStudioAcademy\Commands;


class AttackCommand implements Command
{

    private $playerSpaceship;


    public function __construct($playerSpaceship)
    {
        $this->playerSpaceship = $playerSpaceship;
    }

    public function execute()
    {
        return $this->fight();
    }

    public function fight()
    {

    }
}