<?php


namespace BinaryStudioAcademy\Commands;


class ExceptionCommandHandler
{
    private $command;

    public function __construct($command)
    {
        $this->command = $command;
    }

    public function execute()
    {
        return "Command {$this->command} not found";
    }
}