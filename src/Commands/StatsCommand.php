<?php

namespace BinaryStudioAcademy\Commands;

class StatsCommand implements Command
{
    private $playerShaceship;

    public function __construct($playerShaceship)
    {
        $this->playerShaceship = $playerShaceship;
    }

    public function execute()
    {
      return $this->playerShaceship->renderStats();
    }

}