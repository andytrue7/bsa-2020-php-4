<?php


namespace BinaryStudioAcademy\Spaceships;


class BattleSpaceship extends Spaceship
{
    private $spaceshipName = 'Battle Spaceship';

    public function getSpaceshipName()
    {
        return $this->spaceshipName;
    }
}