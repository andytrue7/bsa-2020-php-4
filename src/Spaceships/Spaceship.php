<?php


namespace BinaryStudioAcademy\Spaceships;


class Spaceship
{
    protected $strength;
    private $armor;
    private $luck;
    private $health;
    private $hold;

    public function getStrength()
    {
        return $this->strength;
    }

    public function setStrength($param)
    {
        $this->strength = $param;
    }

    public function getArmor()
    {
        return $this->armor;
    }

    public function setArmor($param)
    {
        $this->armor = $param;
    }

    public function getLuck()
    {
        return $this->luck;
    }

    public function setLuck($param)
    {
        $this->luck = $param;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($param)
    {
        $this->health = $param;
    }

    public function getHold()
    {
        return $this->hold;
    }
}