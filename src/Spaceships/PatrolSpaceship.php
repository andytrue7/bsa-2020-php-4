<?php

namespace BinaryStudioAcademy\Spaceships;


class PatrolSpaceship extends EnemySpaceships
{
    private $spaceshipName = 'Patrol Spaceship';

    public function getSpaceshipName()
    {
        return $this->spaceshipName;
    }
}