<?php

namespace BinaryStudioAcademy\Spaceships;


class PlayerSpaceship extends Spaceship
{
    private $spaceShipName = 'Player spaceship';

    public function renderStats()
    {
        return 'Spaceship stats:' . PHP_EOL
            . 'strength: ' . ($this->getStrength() ?: 5) . PHP_EOL
            . 'armor: ' . ($this->getArmor() ?: 5) . PHP_EOL
            . 'luck: ' . ($this->getLuck() ?: 5)  . PHP_EOL
            . 'health: ' . ($this->getHealth() ?: 100)   . PHP_EOL
            . 'hold: ' . ($this->getHold() ?: '[ _ _ _ ]') . PHP_EOL;
    }

    public function getSpaceShipName()
    {
        return $this->spaceShipName;
    }
}