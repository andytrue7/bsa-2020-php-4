<?php


namespace BinaryStudioAcademy\Spaceships;


class Executor extends Spaceship
{
    private $spaceshipName = 'Executor';

    public function getSpaceshipName()
    {
        return $this->spaceshipName;
    }
}