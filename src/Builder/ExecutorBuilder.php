<?php


namespace BinaryStudioAcademy\Builder;


use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademy\Spaceships\Executor;

class ExecutorBuilder implements Builder
{

    private $executor;

    public function produceStrength()
    {
        $this->executor->setStrength(Stats::MAX_STRENGTH);
    }

    public function produceArmor()
    {
       $this->executor->setArmor(Stats::MAX_ARMOUR);
    }

    public function produceLuck()
    {
        $this->executor->setLuck(Stats::MAX_LUCK);
    }

    public function produceHealth()
    {
        $this->executor->setHealth(Stats::MAX_HEALTH);
    }

    public function createSpaceship()
    {
        $this->executor = new Executor();
    }

    public function getSpaceship()
    {
        return $this->executor;
    }
}