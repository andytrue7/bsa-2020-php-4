<?php

namespace BinaryStudioAcademy\Builder;

use BinaryStudioAcademy\Spaceships\BattleSpaceship;

class BattleSpaceshipBuilder implements Builder
{
    private $ballteSpaceship;

    private $random;

    public function __construct($random)
    {
        $this->random = $random;
    }

    public function produceStrength()
    {
        $this->ballteSpaceship->setStrength($this->calculateRandomStrength());
    }

    public function produceArmor()
    {
        $this->ballteSpaceship->setArmor($this->calculateRandomArmor());
    }

    public function produceLuck()
    {
        $this->ballteSpaceship->setLuck($this->calculateRandomLuck());
    }

    public function produceHealth()
    {
        $this->ballteSpaceship->setHealth(100);
    }

    public function createSpaceship()
    {
        $this->ballteSpaceship = new BattleSpaceship();
    }

    public function getSpaceship()
    {
        return $this->ballteSpaceship;
    }

    private function calculateRandomStrength()
    {
        return 5 + $this->random * (8 - 5);
    }

    private function calculateRandomArmor()
    {
        return 6 + $this->random * (8 - 6);
    }

    private function calculateRandomLuck()
    {
        return 3 + $this->random * (6 - 3);
    }
}