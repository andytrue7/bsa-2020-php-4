<?php

namespace BinaryStudioAcademy\Builder;


use BinaryStudioAcademy\Spaceships\PatrolSpaceship;

class PatrolSpaceShipBuilder implements Builder
{
    private $patrolSpaceship;

    private $random;

    public function __construct($random)
    {
        $this->random = $random;
    }


    public function produceStrength()
    {
        $this->patrolSpaceship->setStrength($this->calculateRandomStrength());
    }

    public function produceArmor()
    {
        $this->patrolSpaceship->setArmor($this->calculateRandomArmor());
    }

    public function produceLuck()
    {
        $this->patrolSpaceship->setLuck($this->calculateRandomLuck());
    }

    public function produceHealth()
    {
        $this->patrolSpaceship->setHealth(100);
    }

    public function createSpaceship()
    {
        $this->patrolSpaceship = new PatrolSpaceship();
    }

    public function getSpaceShip()
    {
        return $this->patrolSpaceship;
    }

    private function calculateRandomStrength()
    {
        return 3 + $this->random * (4 - 3);
    }

    private function calculateRandomArmor()
    {
        return 2 + $this->random * (4 - 2);
    }

    private function calculateRandomLuck()
    {
        return 1 + $this->random * (2 - 1);
    }
}