<?php

namespace BinaryStudioAcademy\Builder;


interface Builder
{
    public function produceStrength();
    public function produceArmor();
    public function produceLuck();
    public function produceHealth();
    public function getSpaceship();
}