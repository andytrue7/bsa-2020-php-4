<?php

namespace BinaryStudioAcademy\Builder;


class Director
{
    public function build($builder)
    {
        $builder->createSpaceship();
        $builder->produceStrength();
        $builder->produceArmor();
        $builder->produceLuck();
        $builder->produceHealth();

        return $builder->getSpaceShip();
    }
}