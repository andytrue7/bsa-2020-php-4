<?php


namespace BinaryStudioAcademy\Galaxies\BattleGalaxies;


use BinaryStudioAcademy\Galaxies\GalaxyInterface;
use BinaryStudioAcademy\Spaceships\BattleSpaceship;

class Shiar extends BattleGalaxy implements GalaxyInterface
{
    private $galaxyName = 'Shiar';

    public function render()
    {
        return "Galaxy: {$this->getGalaxyName()}." . PHP_EOL
            . "You see a {$this->battleSpaceship->getSpaceshipName()}: " . PHP_EOL
            . 'strength: ' . $this->battleSpaceship->getStrength() . PHP_EOL
            . 'armor: ' . $this->battleSpaceship->getArmor() . PHP_EOL
            . 'luck: ' . $this->battleSpaceship->getLuck() . PHP_EOL
            . 'health: ' . $this->battleSpaceship->getHealth() . PHP_EOL;
    }

    public function getGalaxyName()
    {
        return $this->galaxyName;
    }
}