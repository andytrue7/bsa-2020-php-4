<?php

namespace BinaryStudioAcademy\Galaxies\ExecutorGalaxies;


use BinaryStudioAcademy\Galaxies\GalaxyInterface;

class Isop extends ExecutorGalaxy implements GalaxyInterface
{
    private $galaxyName = 'Isop';

    public function render()
    {
        return "Galaxy: {$this->getGalaxyName()}." . PHP_EOL
            . "You see a {$this->executor->getSpaceshipName()}: " . PHP_EOL
            . 'strength: ' . $this->executor->getStrength() . PHP_EOL
            . 'armor: ' . $this->executor->getArmor() . PHP_EOL
            . 'luck: ' . $this->executor->getLuck() . PHP_EOL
            . 'health: ' . $this->executor->getHealth() . PHP_EOL;
    }

    public function getGalaxyName()
    {
        return $this->galaxyName;
    }
}