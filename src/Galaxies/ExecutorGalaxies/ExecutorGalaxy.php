<?php

namespace BinaryStudioAcademy\Galaxies\ExecutorGalaxies;


class ExecutorGalaxy
{
    protected $executor;

    public function __construct($executor)
    {
        $this->executor = $executor;
    }
}