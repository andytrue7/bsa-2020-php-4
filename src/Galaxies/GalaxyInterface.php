<?php


namespace BinaryStudioAcademy\Galaxies;


interface GalaxyInterface
{
    public function render();
}