<?php


namespace BinaryStudioAcademy\Galaxies;


class HomeGalaxy implements GalaxyInterface
{
    public function render()
    {
        return 'Galaxy: Home Galaxy.' . PHP_EOL;
    }
}