<?php


namespace BinaryStudioAcademy\Galaxies\PatrolGalaxies;


use BinaryStudioAcademy\Galaxies\GalaxyInterface;

class Andromeda extends PatrolGalaxy implements GalaxyInterface
{
    private $galaxyName = 'Andromeda';

    public function render()
    {
        return "Galaxy: {$this->getGalaxyName()}." . PHP_EOL
        . "You see a {$this->patrolSpaceship->getSpaceshipName()}: " . PHP_EOL
        . 'strength: ' . $this->patrolSpaceship->getStrength() . PHP_EOL
        . 'armor: ' . $this->patrolSpaceship->getArmor() . PHP_EOL
        . 'luck: ' . $this->patrolSpaceship->getLuck() . PHP_EOL
        . 'health: ' . $this->patrolSpaceship->getHealth() . PHP_EOL;
    }

    public function getGalaxyName()
    {
        return $this->galaxyName;
    }
}