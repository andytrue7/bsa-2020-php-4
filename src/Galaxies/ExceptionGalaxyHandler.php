<?php

namespace BinaryStudioAcademy\Galaxies;


class ExceptionGalaxyHandler
{
    public function render()
    {
        return 'Nah. No specified galaxy found.';
    }
}