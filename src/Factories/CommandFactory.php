<?php

namespace BinaryStudioAcademy\Factories;

use BinaryStudioAcademy\Commands\ApplyReactorCommand;
use BinaryStudioAcademy\Commands\AttackCommand;
use BinaryStudioAcademy\Commands\BuyCommand;
use BinaryStudioAcademy\Commands\ExceptionCommandHandler;
use BinaryStudioAcademy\Commands\ExitCommand;
use BinaryStudioAcademy\Commands\GalaxyCommand;
use BinaryStudioAcademy\Commands\GrabCommand;
use BinaryStudioAcademy\Commands\HelpCommand;
use BinaryStudioAcademy\Commands\RestartCommand;
use BinaryStudioAcademy\Commands\StatsCommand;
use BinaryStudioAcademy\Commands\WhereAmICommand;
use BinaryStudioAcademy\GalaxyInfo;
use BinaryStudioAcademy\HelpReceiver;
use BinaryStudioAcademy\Spaceships\PlayerSpaceship;

class CommandFactory implements Factory
{
    private $galaxyFactory;

    public function __construct($galaxyFactory)
    {
        $this->galaxyFactory = $galaxyFactory;
    }

    public function createMethod($input)
    {
        [$command, $param] = explode(' ', $input);

        switch ($input) {
            case 'help':
                return new HelpCommand(new HelpReceiver());
                break;
            case 'stats':
                return new StatsCommand(new PlayerSpaceship());
                break;
            case 'set-galaxy ' . $param:
                return new GalaxyCommand($this->galaxyFactory->createMethod($param));
                break;
            case 'attack':
                return new AttackCommand(new PlayerSpaceship());
                break;
            case 'grab':
                return new GrabCommand();
                break;
            case 'buy':
                return new BuyCommand();
                break;
            case 'apply-reactor':
                return new ApplyReactorCommand();
                break;
            case 'whereami':
                return new WhereAmICommand();
                break;
            case 'restart':
                return new RestartCommand();
                break;
            case 'exit':
                return new ExitCommand();
                break;
            default:
                return new ExceptionCommandHandler($command);
                break;
        }
    }
}