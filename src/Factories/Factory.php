<?php

namespace BinaryStudioAcademy\Factories;


interface Factory
{
    public function createMethod($param);

}