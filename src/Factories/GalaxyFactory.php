<?php

namespace BinaryStudioAcademy\Factories;

use BinaryStudioAcademy\Builder\BattleSpaceshipBuilder;
use BinaryStudioAcademy\Builder\Director;;
use BinaryStudioAcademy\Builder\ExecutorBuilder;
use BinaryStudioAcademy\Builder\PatrolSpaceShipBuilder;
use BinaryStudioAcademy\Galaxies\BattleGalaxies\Shiar;
use BinaryStudioAcademy\Galaxies\BattleGalaxies\Xeno;
use BinaryStudioAcademy\Galaxies\ExceptionGalaxyHandler;
use BinaryStudioAcademy\Galaxies\ExecutorGalaxies\Isop;
use BinaryStudioAcademy\Galaxies\HomeGalaxy;
use BinaryStudioAcademy\Galaxies\PatrolGalaxies\Andromeda;
use BinaryStudioAcademy\Galaxies\PatrolGalaxies\Pegasus;
use BinaryStudioAcademy\Galaxies\PatrolGalaxies\Spiral;

class GalaxyFactory implements Factory
{
    private $random;

    public function __construct($random)
    {
        $this->random = $random;
    }

    public function createMethod($galaxy)
    {
        switch ($galaxy) {
            case 'home':
                return new HomeGalaxy();
                break;
            case 'andromeda':
                return new Andromeda((new Director())->build(new PatrolSpaceShipBuilder($this->random)));
                break;
            case 'spiral':
                return new Spiral((new Director())->build(new PatrolSpaceShipBuilder($this->random)));
                break;
            case 'pegasus':
                return new Pegasus((new Director())->build(new PatrolSpaceShipBuilder($this->random)));
                break;
            case 'shiar':
                return new Shiar((new Director())->build(new BattleSpaceshipBuilder($this->random)));
                break;
            case 'xeno':
                return new Xeno((new Director())->build(new BattleSpaceshipBuilder($this->random)));
                break;
            case 'isop':
                return new Isop((new Director())->build(new ExecutorBuilder()));
                break;
            default:
                return new ExceptionGalaxyHandler();
                break;
        }
    }
}